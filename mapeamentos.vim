let mapleader=";"
" Salvar arquivo
noremap <C-z> :update<CR>
inoremap <C-z> <ESC>:update<CR>
vnoremap <C-z> <ESC>:update<CR>
" Navegação entre painéis (splits)
map <c-h> <c-w>h
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
" Navegação entre abas
map <leader>n <esc>:tabp<cr>
map <leader>m <esc>:tabn<cr>
" Ordem crescente
vnoremap <leader>s :sort<cr>
" Identação de código
vnoremap < <gv
vnoremap > >gv
" ativa o corretor ortográfico
map <F6> :setlocal spell! spelllang=pt<CR>
" Altera o número da linha de acordo com o modo
:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber " modo relativo no modo NORMAL e VISUAL 
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber " modo absoluto no modo INSERT
:augroup END
inoremap <space><space> <esc>/<++><enter>"_c4l
autocmd Filetype html inoremap ;i <em></em><space><++><esc>FeT>i
autocmd Filetype html inoremap ;p <p></p><space><cr><++><esc>FpT>i
autocmd Filetype html inoremap ;b <strong></strong><space><++><esc>FgT>i
