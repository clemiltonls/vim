" Permite aplicação automática das alterações do arquivo de configuração
autocmd! bufwritepost .vimrc source %
set number relativenumber "exibe número(híbridos) das linhas
set tw=79 " Limita caracteres na coluna 79
set nowrap " quebra linha automaticamente
set fo-=t " quebra a linha no modo de inserção
set colorcolumn=80 "exibe coluna limitadora na coluna 80
set history=700 "define histórico de modificações
set undolevels=700 "define quantidade de ações desfazer
set tabstop=4 "define tamanho da tabulação
set softtabstop=4 "define tamanho da tabulação
set shiftwidth=4 "define tamanho da tabulação
set et
set shiftround " define uso de espaços no lugar de tabulação
set expandtab " define uso de espaços no lugar de tabulação
set hlsearch " destaca as buscas
set incsearch " inclui pedaços de textos nas buscas
set ignorecase " ignora tamanho da caixa
set smartcase " ignora notação camelo
set nobackup " não faz backup automático
set nowritebackup " não faz backup automático
set noswapfile " não faz backup automático
set pastetoggle=<F2> " Define melhor funcionamento do recurso de copiar e colar
set clipboard=unnamed " Permite copia e cola em ambas as direções
set bs=2 " Melhora o funcionamento do backspace
" Mapeamento de teclas
so ~/.vim/mapeamentos.vim " arquivo separado para mapeamentos de teclado
set path+=** " permite busca de arquivo em todo o sistema
set wildmenu " permite o uso do menu inferior
" necessário para funcionamento do vundle
set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
so ~/.vim/plugins.vim " arquivo separado com a lista de plugins
call vundle#end()
set nocompatible
filetype plugin indent on
syntax on " fim funcionamento do vundle
let g:airline_powerline_fonts = 1 " Aparência do Powerline
let g:airline_theme="angr" " Tema do airline
let g:airline#extensions#tabline#enabled = 1 "Tema do airline nas abas
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_guide_size = 1
set laststatus=2 " Exibe barra de status inferior
set foldmethod=indent " minimizar blocos de código
" Ativa colorização de espaços em branco inúteis
autocmd ColorScheme * highlight ExtraWhitespace guibg=#993300
au InsertLeave * match ExtraWhitespace /\s\+$/
colorscheme iceberg "esquema de cores
set guifont=Fira\ Code\ Bold\ 12
