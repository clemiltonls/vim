# Arquivo de Configuração do Vim/gVim
## Autor: Clemilton Lima de Souza
Como fazer para funcionar:  
No Linux:  
1 - Renomeie o diretório do repositório para .vim dentro do diretório home do
seu usuário;  
2 - No terminal
* Renomeie o arquivo vimrc para ~/.vimrc
* Crie o diretório bundle e entre nele
* Rode o seguinte comando: git clone https://github.com/VundleVim/Vundle.vim
* Abra o vim e rode o comando no editor: :PluginInstall  


3 - Enjoy
